local hud
local channel_name
local frame
local total_frames = 8
local frame_timer

local function set_texture(texture)
	if hud then
		minetest.localplayer:hud_change(hud, "text", texture)
	end
end

minetest.register_on_modchannel_message(function(cname, sender, message)
	if sender == "" and cname == channel_name then
		if message == "start" then
			if not frame_timer then
				frame = 0
				frame_timer = 0
			end
		elseif message == "stop" then
			frame_timer = nil
			set_texture("blank.png")
		else
			total_frames = tonumber(message) or 8
		end
	end
end)

minetest.register_globalstep(function(dtime)
	local player = minetest.localplayer

	if player and not hud then
		hud = player:hud_add({
			hud_elem_type = "image",
			position = {x = 0.5, y = 0.5},
			scale = {x = -100, y = -100},
			text = "blank.png",
			z_index = 1000,
		})

		channel_name = "mcl_burning:" .. player:get_name()
		minetest.mod_channel_join(channel_name)
	end

	if frame_timer then
		frame_timer = frame_timer - dtime

		if frame_timer <= 0 then
			frame_timer = 1.0 / total_frames
			frame = (frame + 1) % total_frames

			set_texture("mcl_burning_hud_flame_animated.png^[opacity:180^[verticalframe:" .. total_frames .. ":" .. frame)
		end
	end
end)
